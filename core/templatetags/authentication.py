# -*- coding: utf-8 -*-
from django import template
from django.core.urlresolvers import reverse

register = template.Library()
@register.inclusion_tag('auth_menu.html', takes_context = True)
def auth_menu(context):
    request = context.get('request', None)
    if request:
        items = []
        prefix = ''
        auth_status = request.user.is_authenticated()
        if auth_status:
            prefix = u'Вошли как %s' % request.user.username
            items.append({
                'url': reverse('logout'),
                'title': u'Выйти',
            })
        else:
            items.append({
                'url': reverse('login', kwargs={'template_name': 'login.html'}),
                'title': u'Войти',
            })
            
        return {
            'prefix': prefix,
            'items': items,
        }

