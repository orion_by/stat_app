# -*- coding: utf-8 -*-
from django import forms
from django.db import models

class VisitorsPerSomeDaysFilters(forms.Form):
    start = forms.DateField()
    end   = forms.DateField()

    # идентификаторы просматриваемых материалов
    nid1  = forms.CharField(
        max_length = 255,
        required   = False,
        widget     = forms.TextInput(
            attrs = {
                'class': 'vpsd-filters-form__nid',
                'size' : 100,
            },
        ),
    )
    nid2  = nid1
    nid3  = nid1
    nid4  = nid1
    nid5  = nid1
    nid6  = nid1
    nid7  = nid1
    nid8  = nid1
    nid9  = nid1
    nid10 = nid1

class ConvercionNewUsersToActiveFilters(forms.Form):
    start_first_enter = forms.DateField()
    end_first_enter   = forms.DateField()

    start_activity = forms.DateField()
    end_activity   = forms.DateField()

class CountNewActiveUsersByDayFilters(forms.Form):
    start_first_enter = forms.DateField()
    end_first_enter   = forms.DateField()

