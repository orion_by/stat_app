# coding=utf-8
import re
import json
import datetime
from django.http import HttpResponse


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def jsonify(data):
    """
    jsonify with support for MongoDB ObjectId
    """
    return json.dumps(data, cls=JsonEncoder)


def valid_ip(ip_str):
    if ip_str:
        pattern = r'(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
        if re.search(pattern, ip_str):
            return True
    return False


def json_response(data):
    return HttpResponse(jsonify(data), content_type="application/json")


def percents(part, full):
    return round(part*1.0 / full * 100, 2)


actions = {
    'scroll_to_end' : u'одиночную новость прочитали(проскролили) полностью',
    'open_left_menu' : u'открыли левое меню',
    'open_right_menu' : u'открыли правое меню',
    'click:linked_news' : u'нажали на связанную новость под одиночной новостью',
    'click:start_test' : u'нажали ПРОЙТИ ТЕСТ (IQ или КРЕАТИВНОСТЬ)',
    'click:showResults' : u'нажали ПОСМОТРЕТЬ РЕЗУЛЬТАТ (IQ или КРЕАТИВНОСТЬ)',
    'click:iq_test' : u'нажали для перехода к IQ тесту',
    'click:instr' : u'нажали ИНСТРУКЦИЯ (IКРЕАТИВНОСТЬ)',
    'end_test' : u'прошли тест (IQ или КРЕАТИВНОСТЬ)',
    'click:fill_form' : u'нажали заполнить анкету в тесте',
    'click:menu_like' : u'поделится приложением(мне нравится)',
    'click:later' : u'в диалогах оценить и обновить версию кнопка позже',
    'click:\d+' : u'клик на новость с id ',
    'click:send_to_innoros' : u'написать в редакцию(кнопка с карандашом)',
    'click:promo_braintest' : u'клик по баннеру брейн',
    'click_share' : u'кликнули иконку поделится новостью',
    'gcm_message_notification' : u'показ уведомления о новом сообщении',
    'open_from_notification' : u'открыто сообщение через уведомление',
    'gcm_message_notification_open' : u'открыли сообщение из уведомления до 2.6.2',
    'click:in_app_link' : u'клик на внутреннюю ссылку',
    'open_news_day_from_notification' : u'открыли новость дня из уведомления',
    'news_day_notification' : u'показ уведомления о новости дня',
    'screen_shows' : u'эксперементальный анналог sendView (в 100 версии только для новостей)',
    'click:send_answer' : u'нажали кнопку ОТПРАВИТЬ в СООБЩЕНИЕ (ответили)',
    'send_new_message' : u'написать в редакцию(указывается экран откуда нажали)',
    'click: animation_[trye/false]' : u'нажали кнопку "Анимация"',
    'click: notifications_is_show_[trye/false]' : u'нажали кнопку "Оперативные уведомления"',
    'click: gsm_image_loader_[trye/false]' : u'нажали кнопку "Медиа файлы"',
    'click: on_off_update_intervale_[trye/false]' : u'нажали кнопку "Ежедневные оповещения"',
    'click: font_size_[VALUE]' : u'нажали кнопку "Шрифт"',
    'click: loading_count_[VALUE]' : u'нажали кнопку "Новости (количество подгружаемых...)"',
    'click: background_color_[VALUE]' : u'нажали кнопку "Цвет фона"',
    'click: notifications_is_sound_[trye/false]' : u'нажали кнопку "Звук при уведомлении"',
    'click: notifications_new_message_ringtone_[VALUE]' : u'нажали кнопку "Выберите звук уведомления"',
    'click: [TYPE_NEWS]_[trye/false]' : u'подписались/отписались для уведомлений от этого типа новостей',
    'click_share: [APP]' : u'Выбрали приложение для шаринга (в диалоге)',
}