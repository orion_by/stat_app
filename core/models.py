# -*- coding: utf-8 -*-
from django.db import models


class Viewstat(models.Model):
    class Meta():
        managed = False
        db_table = 'viewstat'

    uid         = models.AutoField(primary_key=True)
    user_id     = models.IntegerField(null=True)
    email       = models.CharField(max_length=255)
    resolution  = models.CharField(max_length=255, blank=True)
    device_type = models.CharField(max_length=255, blank=True)
    device_name = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    token       = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)
    version     = models.IntegerField()

class ViewstatContent(models.Model):
    class Meta():
        managed = False
        db_table = 'viewstat_content'

    id          = models.IntegerField(primary_key=True)
    project     = models.CharField(max_length=255, blank=True)
    user_id     = models.IntegerField()
    nid         = models.CharField(max_length=255, blank=True)
    view_date   = models.CharField(max_length=255, blank=True)
    viewed_time = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)

class ViewstatEvent(models.Model):
    class Meta():
        managed = False
        db_table = 'viewstat_event'

    id           = models.IntegerField(primary_key=True)
    project      = models.CharField(max_length=255, blank=True)
    user_id      = models.IntegerField()
    type_event   = models.CharField(max_length=255, blank=True)
    view_date    = models.CharField(max_length=255, blank=True)
    screen_event = models.CharField(max_length=255, blank=True)
    time_send    = models.CharField(max_length=255, blank=True)
    ip           = models.CharField(max_length=255, blank=True)

class ViewstatContentWeek(models.Model):
    '''
    Данные из viewstat_content за последнюю неделю
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_content_week'

    id          = models.IntegerField(primary_key=True)
    project     = models.CharField(max_length=255, blank=True)
    user_id     = models.IntegerField()
    nid         = models.CharField(max_length=255, blank=True)
    view_date   = models.CharField(max_length=255, blank=True)
    viewed_time = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)

class ViewstatContent2Weeks(models.Model):
    '''
    Данные из viewstat_content за последние 2 недели
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_content_2weeks'

    id          = models.IntegerField(primary_key=True)
    project     = models.CharField(max_length=255, blank=True)
    user_id     = models.IntegerField()
    nid         = models.CharField(max_length=255, blank=True)
    view_date   = models.CharField(max_length=255, blank=True)
    viewed_time = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)

class ViewstatContent3Weeks(models.Model):
    '''
    Данные из viewstat_content за последние 3 недели
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_content_3weeks'

    id          = models.IntegerField(primary_key=True)
    project     = models.CharField(max_length=255, blank=True)
    user_id     = models.IntegerField()
    nid         = models.CharField(max_length=255, blank=True)
    view_date   = models.CharField(max_length=255, blank=True)
    viewed_time = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)

class ViewstatContent40Days(models.Model):
    '''
    Данные из viewstat_content за последние 40 дней
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_content_40days'

    id          = models.IntegerField(primary_key=True)
    project     = models.CharField(max_length=255, blank=True)
    user_id     = models.IntegerField()
    nid         = models.CharField(max_length=255, blank=True)
    view_date   = models.CharField(max_length=255, blank=True)
    viewed_time = models.CharField(max_length=255, blank=True)
    time_send   = models.CharField(max_length=255, blank=True)
    ip          = models.CharField(max_length=255, blank=True)

class ViewstatEventWeek(models.Model):
    '''
    Данные из viewstat_event за последнюю неделю
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_event_week'

    id           = models.IntegerField(primary_key=True)
    project      = models.CharField(max_length=255, blank=True)
    user_id      = models.IntegerField()
    type_event   = models.CharField(max_length=255, blank=True)
    view_date    = models.CharField(max_length=255, blank=True)
    screen_event = models.CharField(max_length=255, blank=True)
    time_send    = models.CharField(max_length=255, blank=True)
    ip           = models.CharField(max_length=255, blank=True)

class ViewstatEvent2Weeks(models.Model):
    '''
    Данные из viewstat_event за последние 2 недели
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_event_2weeks'

    id           = models.IntegerField(primary_key=True)
    project      = models.CharField(max_length=255, blank=True)
    user_id      = models.IntegerField()
    type_event   = models.CharField(max_length=255, blank=True)
    view_date    = models.CharField(max_length=255, blank=True)
    screen_event = models.CharField(max_length=255, blank=True)
    time_send    = models.CharField(max_length=255, blank=True)
    ip           = models.CharField(max_length=255, blank=True)

class ViewstatEvent3Weeks(models.Model):
    '''
    Данные из viewstat_event за последние 3 недели
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_event_3weeks'

    id           = models.IntegerField(primary_key=True)
    project      = models.CharField(max_length=255, blank=True)
    user_id      = models.IntegerField()
    type_event   = models.CharField(max_length=255, blank=True)
    view_date    = models.CharField(max_length=255, blank=True)
    screen_event = models.CharField(max_length=255, blank=True)
    time_send    = models.CharField(max_length=255, blank=True)
    ip           = models.CharField(max_length=255, blank=True)

class ViewstatEvent40Days(models.Model):
    '''
    Данные из viewstat_event за последние 40 дней
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_event_40days'

    id           = models.IntegerField(primary_key=True)
    project      = models.CharField(max_length=255, blank=True)
    user_id      = models.IntegerField()
    type_event   = models.CharField(max_length=255, blank=True)
    view_date    = models.CharField(max_length=255, blank=True)
    screen_event = models.CharField(max_length=255, blank=True)
    time_send    = models.CharField(max_length=255, blank=True)
    ip           = models.CharField(max_length=255, blank=True)

class ViewstatActivityOfNewUsers(models.Model):
    '''
    Статистика, кто сколько посещал экранов за какой-то период после первого вхождения.
    '''
    class Meta():
        managed = False
        db_table = 'viewstat_activity_of_new_users'

    id                       = models.IntegerField(primary_key=True)
    user_id                  = models.IntegerField()
    first_enter_date         = models.DateField()
    showed_displays_30days   = models.IntegerField()
    showed_displays_all_time = models.IntegerField()
