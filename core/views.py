# coding=utf-8
import json
import time
import operator
import csv
import re

from re import match, search
from datetime import datetime, timedelta
from oauth2client.client import AccessTokenRefreshError
from django.shortcuts import HttpResponse, render_to_response
from django.db import connection
from django.db.models import Count
from models import Viewstat, ViewstatContent, ViewstatEvent, ViewstatContentWeek, ViewstatContent2Weeks, ViewstatContent3Weeks, ViewstatContent40Days,  ViewstatEventWeek, ViewstatEvent2Weeks, ViewstatEvent3Weeks, ViewstatEvent40Days, ViewstatActivityOfNewUsers
from django.contrib.auth.decorators import login_required
from forms import VisitorsPerSomeDaysFilters, ConvercionNewUsersToActiveFilters, CountNewActiveUsersByDayFilters
from django.template import RequestContext

from utils import valid_ip, json_response, percents
from ga import api_v3_auth


# @login_required
def stat_menu(request):
    return render_to_response('stat_menu.html')


# @login_required
def news_per_day(request):
    return render_to_response('pages/news_per_day.html')


# @login_required
def news_per_day_data(request):
    period = request.GET.get('period', 1)
    sql = """
    SELECT count(user_id), node_count
    FROM (SELECT user_id, count(*) node_count
          FROM viewstat_event
          WHERE type_event REGEXP 'click:[[:digit:]]+' AND
                time_send > DATE_SUB( NOW( ) , INTERVAL %s DAY )
          GROUP BY user_id) T
    GROUP BY node_count
    LIMIT 0, 10;
    """ % period
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    for d in data:
        print d
    return json_response(data)


# @login_required
def in_a_row(request):
    return render_to_response('pages/in_a_row.html')


# @login_required
def in_a_row_data(requset):
    period = requset.GET.get('period', 7)
    sql = """
    SELECT user_id, COUNT(DISTINCT DATE(time_send)) days
    FROM viewstat_event WHERE time_send > DATE_SUB( NOW( ) , INTERVAL {0} DAY )
    GROUP BY user_id
    HAVING days = {0};
    """.format(period)
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    return json_response({'length': len(data)})


# @login_required
def add_user(request):
    data = request.POST
    email = data.get('email')
    token = data.get('token')

    if email and token:
        ip = data.get('ip')
        ip = ip if valid_ip(ip) else '0.0.0.0'
        exist_users = list(Viewstat.objects.filter(email=email).order_by('user_id'))
        if not exist_users:
            user = Viewstat(
                email=email,
                resolution=data.get('resolution'),
                device_type=data.get('device_type'),
                device_name=data.get('device_name'),
                time_send=int(time.time()),
                token=token,
                ip=ip if valid_ip(ip) else '0.0.0.0',
                version=data.get('version', 0),
                user_id=0
            )
            user.save()
            user.user_id = user.uid
            user.save()
            response = {'user_id': user.user_id}
        else:
            if len(token) > 30:
                exist_users[-1].token = token
                exist_users[-1].save()
            response = {'user_id': exist_users[-1].user_id}
    else:
        response = {
            'status': 'error',
            'where': 'inituser',
            'message': 'empty POST[email] or empty POST[token]'
        }
    return HttpResponse(json.dumps(response), content_type="application/json")


# @login_required
def add_event(request):
    data = request.POST.get('event_data')
    if data:
        for event in json.loads(data):
            try:
                user_id = int(event.get('user_id', 0))
            except ValueError:
                continue
            ip = event.get('ip')
            new_content = ViewstatEvent(
                project=event.get('project', None),
                user_id=user_id,
                type_event=event.get('type_event', None),
                screen_event=event.get('screen_event', None),
                view_date=event.get('view_date', None),
                time_send=int(time.time()),
                ip=ip if valid_ip(ip) else '0.0.0.0'
            )
            new_content.save()
        response = True
    else:
        response = {
            'status': 'error',
            'where': 'inputevent',
            'message': 'empty POST[event_data]'
        }
    return HttpResponse(json.dumps(response), content_type="application/json")


# @login_required
def add_content(request):
    data = request.POST.get('viewed_data')
    if data:
        for content in json.loads(data):
            try:
                user_id = int(content.get('user_id', 0))
            except ValueError:
                continue
            ip = content.get('ip')
            new_content = ViewstatContent(
                user_id=user_id,
                project=content.get('project', None),
                nid=content.get('nid', 0),
                view_date=content.get('view_date', None),
                viewed_time=content.get('viewed_time', None),
                time_send=int(time.time()),
                ip=ip if valid_ip(ip) else '0.0.0.0'
            )
            new_content.save()
        response = True
    else:
        response = {
            'status': 'error',
            'where': 'input',
            'message': 'empty POST[viewed_data]'
        }
    return HttpResponse(json.dumps(response), content_type="application/json")


# @login_required
def top_enters(request):
    return render_to_response('pages/top_enters.html')


# @login_required
def top_enters_data(request):
    period = request.GET.get('period', 7)
    def get_results(service, profile_id):
        end_date = datetime.now()
        start_date = end_date - timedelta(days=int(period))
        end_date.strftime('%Y-%m-%d')
        return service.data().ga().get(
            dimensions='ga:landingScreenName',
            ids='ga:' + profile_id,
            start_date=start_date.strftime('%Y-%m-%d'),
            end_date=end_date.strftime('%Y-%m-%d'),
            metrics='ga:visitors',
            sort='-ga:visitors',
            max_results='20',
        ).execute()

    service = api_v3_auth.initialize_service()
    results = {}
    try:
        profile_id = api_v3_auth.get_first_profile_id(service)
        if profile_id:
            results = get_results(service, profile_id)
    except TypeError, error:
        results['error'] = 'There was an error in constructing your query : %s' % error
    except AccessTokenRefreshError:
        results['error'] = 'The credentials have been revoked or expired, please re-run the application to re-authorize'
    return json_response({'data': results['rows']})


# @login_required
def visits_per_day(request):
    cursor = connection.cursor()
    user_stats = {}
    for days_count in xrange(1, 8):
        sql = """
        SELECT user_id, COUNT(DISTINCT CONCAT(DAY(time_send), ' : ' ,HOUR(time_send) )) visits
        FROM viewstat_event_d
        WHERE time_send > NOW() - INTERVAL {0} DAY
        GROUP BY user_id
        """.format(days_count)
        cursor.execute(sql)
        data = cursor.fetchall()
        for row in data:
            user_stats.setdefault(row[0], []).append(row[1])
    stats = {}
    for visits in user_stats.values():
        avg = round(sum(visits) / float(len(visits)), 1)
        if not stats.get(avg):
            stats[avg] = 1
        else:
            stats[avg] += 1
    stats = sorted(stats.iteritems(), key=operator.itemgetter(1), reverse=True)
    return render_to_response('visits_per_day.html', {'data': stats})


# @login_required
def active_users(request):
    return render_to_response('pages/active_users.html')


# @login_required
def active_users_data(request):
    period = request.GET.get('period', 7)
    sql = """
        SELECT DATE(time_send) day, count(DISTINCT user_id)
        FROM viewstat_event_d
        WHERE time_send > NOW() - INTERVAL {0} DAY
        GROUP BY DATE(time_send)
        ORDER BY day DESC
    """.format(period)
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    return json_response(data)


# @login_required
def inactive_users(request):
    sql ="""
    SELECT email, group_concat(distinct active) active
    FROM viewstat
    WHERE email != '' AND
          email != '-'
    GROUP BY email
    HAVING active != '1,0' AND active != '1' AND active != '0,1'
    """
    cursor = connection.cursor()
    cursor.execute(sql)
    inactive_users_list = cursor.fetchall()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="inactive_users.csv"'
    writer = csv.writer(response)
    for user_email in inactive_users_list:
        writer.writerow([user_email[0]])
    return response


# @login_required
def actions_random_user(request):
    random_users_sql = """
    SELECT user_id
    FROM viewstat_event_d
    WHERE time_send > NOW() - INTERVAL 1 DAY
    ORDER BY RAND()
    LIMIT 5
    """

    cursor = connection.cursor()
    cursor.execute(random_users_sql)
    random_users = map(lambda x: x[0], cursor.fetchall())
    return render_to_response('pages/actions_random_user.html', {'users':random_users})


# @login_required
def actions_random_user_data(request):
    from utils import actions
    user = request.GET.get('user', '')
    sql = """
    SELECT type_event, screen_event, id
    FROM viewstat_event_d
    WHERE user_id = {0} AND
          time_send > NOW() - INTERVAL 1 DAY
    ORDER BY id
    """.format(user)
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    result = []
    for row in data:
        action = None
        for key in actions.keys():
            if match(key, row[0]):
                action = actions[key]
                if key == 'click:\d+':
                    action += search('\d+', row[0]).group()
        result.append((action or row[0], row[1], row[2]))
    return json_response(result)


# @login_required
def new_users_stat(request):
    return render_to_response('pages/new_users_stat.html')


# @login_required
def new_users_stat_data(request):
    date = request.GET.get('date')
    period = int(request.GET.get('period', 0))
    response = {'rows': []}
    if date and period:
        date_start = datetime.strptime(date, '%Y-%m-%d')
        date_end = date_start + timedelta(days=period)
        sql = """
        SELECT uid
        FROM viewstat
        WHERE DATE(FROM_UNIXTIME(time_send)) = '{0}'
        """.format(date)
        cursor = connection.cursor()
        cursor.execute(sql)
        users = map(lambda x: x[0], cursor.fetchall())
        count_users = len(users)
        response['count_users'] = count_users
        sql = """
        SELECT count(user_id), days
        FROM (SELECT t1.user_id, COUNT(DISTINCT DATE(t1.time_send)) days
                FROM viewstat_event t1
                    INNER JOIN viewstat t2
                    ON t1.user_id = t2.uid
                WHERE t1.time_send >= '{0}' AND
                        t1.time_send <= '{1}' AND
                        DATE(FROM_UNIXTIME(t2.time_send)) = '{0}'
                GROUP BY t1.user_id) T
        GROUP BY days
        ORDER BY days
        """.format(date_start, date_end)
        cursor.execute(sql)
        data = cursor.fetchall()
        total_users = reduce(lambda res, x: res + x[0], data, 0)
        response['rows'].append((u'не заходили не разу', percents(count_users - total_users, count_users)))

        map(lambda row: response['rows'].append((u'заходили %s раз' % row[1], percents(row[0], count_users))), data)

    return json_response(response)


# @login_required
def unique_users(request):
    return render_to_response('pages/unique_users.html')


# @login_required
def unique_users_data(request):
    sql = """
    SELECT count(DISTINCT user_id)
    FROM viewstat_event
    WHERE WHERE DATE(FROM_UNIXTIME(time_send)) > NOW() - INTERVAL 30 DAY
    """
    sql2 = """
    SELECT user_id, COUNT(DISTINCT DATE(time_send)) days
    FROM viewstat_event WHERE time_send > DATE_SUB( NOW( ) , INTERVAL {0} DAY )
    GROUP BY user_id
    HAVING days = {0};
    """
    cursor = connection.cursor()
    response = {'row': []}
    for days in (15, 30, 60):
        cursor.execute(sql.format(days))
        data = cursor.fetchall()
        response['row'].append(data[0][0])
    return json_response(response)


# @login_required
def debug(request):
    sql = """
    SELECT count(user_id), days FROM
      (SELECT t1.user_id user_id, COUNT(DISTINCT DATE(t1.time_send)) days
        FROM viewstat_event_d t1
            INNER JOIN (SELECT DISTINCT(user_id)
                            FROM viewstat_event_d
                            WHERE time_send > NOW() - INTERVAL 30 DAY) t2
            ON t2.user_id = t1.user_id
        WHERE time_send > NOW() - INTERVAL 30 DAY
        GROUP BY t1.user_id) t
    GROUP BY days
    """
    cursor = connection.cursor()
    response = {'row': []}
    cursor.execute(sql)
    data = cursor.fetchall()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="inactive_users.csv"'
    writer = csv.writer(response)
    for row in data:
        writer.writerow([user_email[0]])
    return response

# @login_required
def visitors_per_some_days(request):
    '''
    Заросы, требующиеся при отладке:
    SELECT COUNT(*), id, nid FROM viewstat_content_week WHERE time_send>DATE_SUB( NOW( ) , INTERVAL 1 DAY) GROUP BY nid ORDER BY time_send ASC 
    nids: 26911, 26916
    '''
    timer = time.time()

    counts = []
    results = {}
    cross = None
    filters_form = VisitorsPerSomeDaysFilters(initial = {
        'start': '2014-06-23',
        'end': '2014-06-26',
    })

    if request.method == 'POST':
        start = request.POST.get('start', None)
        end   = request.POST.get('end', None)
        if (not start) or (not end):
            assert False
        # переводим дату в объект даты
        start = datetime.strptime(start, '%Y-%m-%d')
        end   = datetime.strptime(end, '%Y-%m-%d')

        nids = []
        uids = []
        # TODO сделать более профессионально 
        for i in range(1, 11):
            fname = 'nid{0}'.format(i)
            value = request.POST.get(fname, None)
            if value:
                # reg = re.compile("\[nid:([^\]]+)\]")
                matches = re.search(ur"\[nid:([^\]]+)\]", value)
                # matches = reg.match(value)
                try:
                    nid = matches.group(1)
                except:
                    nid = None
                if nid:
                    nids.append(nid)
                    base_queryset = get_queryset_for_period(model_type = 'viewstat_event', start = start)
                    queryset = base_queryset.filter(
                        time_send__range = [start, end],
                        # nid = nid,
                        type_event = u'click:{0}'.format(nid),
                    )
                    queryset_uids = set([row.user_id for row in queryset])
                    # uids.append(queryset_uids)
                    if cross == None:
                        cross = queryset_uids
                    else:
                        cross = cross & queryset_uids

                    results[fname] = {
                        'label'       : fname,
                        'count'       : len(queryset_uids),
                        'cross_count' : len(cross)
                    }

        log = []
        # if uids:
        #     cross = uids[1]
        #     for row in uids:
        #         cross = cross & row
        # assert False


        filters_form = VisitorsPerSomeDaysFilters(request.POST)
    if cross == None:
        cross == set()

    timer =  str(time.time() - timer)

    return render_to_response('pages/visitors_per_some_days.html', {
        'results'      : results,
        'filters_form' : filters_form,
    }, context_instance = RequestContext(request))

def get_queryset_for_period(model_type, start):
    today       = datetime.today()
    week        = timedelta(weeks = 1)
    two_weeks   = timedelta(weeks = 2)
    three_weeks = timedelta(weeks = 3)
    days40      = timedelta(days = 40)

    if start > (today - week):
        if model_type == 'viewstat_content':
            return ViewstatContentWeek.objects.all()
        elif model_type == 'viewstat_event':
            return ViewstatEventWeek.objects.all()
    elif start > (today - two_weeks):
        if model_type == 'viewstat_content':
            return ViewstatContent2Weeks.objects.all()
        elif model_type == 'viewstat_event':
            return ViewstatEvent2Weeks.objects.all()
    elif start > (today - three_weeks):
        if model_type == 'viewstat_content':
            return ViewstatContent3Weeks.objects.all()
        elif model_type == 'viewstat_event':
            return ViewstatEvent3Weeks.objects.all()
    elif start > (today - days40):
        if model_type == 'viewstat_content':
            return ViewstatContent40Days.objects.all()
        elif model_type == 'viewstat_event':
            return ViewstatEvent40Days.objects.all()

# @login_required
def convercion_new_users_to_active(request):
    '''
    Конверсия новых пользователей в активных.
    '''
    results = {}
    full_count_users = None
    filters_form = ConvercionNewUsersToActiveFilters(initial = {
        'start_first_enter': '2014-06-02',
        'end_first_enter': '2014-06-29',
        'start_activity': '2014-06-22',
        'end_activity': '2014-06-29',
    })

    if request.method == 'POST':
        filters_form = ConvercionNewUsersToActiveFilters(request.POST)
        # Период, в котором должен был быть первый вход в приложение 
        first_enter_period = [
            datetime.strptime( request.POST.get('start_first_enter'), '%Y-%m-%d' ),
            datetime.strptime( request.POST.get('end_first_enter') + ' 23:59:59',   '%Y-%m-%d %H:%M:%S' ),
        ]
        activity_period = [
            datetime.strptime( request.POST.get('start_activity'), '%Y-%m-%d' ),
            datetime.strptime( request.POST.get('end_activity') + ' 23:59:59',   '%Y-%m-%d %H:%M:%S' ),
        ]

        first_enter_period_timestamp = [time.mktime(d.timetuple()) for d in first_enter_period]

        # получаем пользователей, впервые вошедних в указанный период
        users = Viewstat.objects.only('user_id').filter(time_send__range = first_enter_period_timestamp)
        # users = Viewstat.objects.filter(time_send__range = ['1385049884', '1385049896'])
        uids = set([u.user_id for u in users])

        # смотрим, кто из найденных пользователей смотрел хоть что-то в указанные даты
        base_queryset = get_queryset_for_period('viewstat_content', activity_period[0])
        base_queryset = base_queryset.filter(user_id__in = uids, time_send__range = activity_period)
        count_displays = base_queryset.filter(user_id__in = uids, time_send__range = activity_period).values('user_id').annotate(count_nids = Count('user_id'))
        for r in count_displays:
            count = r['count_nids']
            if count in results:
                results[count] += 1
            else:
                results[count] = 1

        full_count_users = base_queryset.values('user_id').distinct().count()

    return render_to_response('pages/convercion_new_users_to_active.html', {
        'results'          : results,
        'full_count_users' : full_count_users,
        'filters_form'     : filters_form,
    }, context_instance = RequestContext(request))

# @login_required
def count_new_active_users_by_day(request):
    '''
    Позволяет увидеть, сколько пользователей, которые в последствии пользуются приложением,
    появляются каждый день указанного периода.
    '''
    results = []
    filters_form = CountNewActiveUsersByDayFilters(initial = {
        'start_first_enter': '2014-05-01',
        'end_first_enter': '2014-06-02',
    })

    if request.method == 'POST':
        filters_form = CountNewActiveUsersByDayFilters(request.POST)
        # Период, в котором должен был быть первый вход в приложение 
        start_first_enter_period = request.POST.get('start_first_enter')
        end_first_enter_period = request.POST.get('end_first_enter') + ' 23:59:59'
        first_enter_period = [
            datetime.strptime( start_first_enter_period, '%Y-%m-%d' ),
            datetime.strptime( end_first_enter_period,   '%Y-%m-%d %H:%M:%S' ),
        ]

        sql = """
        SELECT id, `first_enter_date`, COUNT(`user_id`) user_id__count
        FROM
        `viewstat_activity_of_new_users`
        WHERE
        `showed_displays_30days` > 1
        AND
        (`first_enter_date` BETWEEN '%s' AND '%s')
        GROUP BY DATE(`first_enter_date`)
        ORDER BY `first_enter_date`
        """ % (start_first_enter_period, end_first_enter_period,)

        results = ViewstatActivityOfNewUsers.objects.raw(sql)

    return render_to_response('pages/count_new_active_users_by_day.html', {
        'results'          : results,
        'filters_form'     : filters_form,
    }, context_instance = RequestContext(request))
