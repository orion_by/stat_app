$( document ).ready(function() {
    $('#refresh').on('click',
        function(){
            var alert_template = _.template($('#refresh_alert').html());
            localStorage.clear();
            $("#alert_box").html(alert_template())
        }
    );
});