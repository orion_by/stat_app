$( document ).ready(function() {
    $('.nav-pills li').on('click',
        function(e){
            $('.active').removeClass('active');
            $(e.target).parent().addClass('active');
            load_data();
        }
    );
    load_data()
});

function load_data(){
    var data_template = _.template($('#unique_users').html());
    var loading_template = _.template($('#loading').html());
    var space = $('#space');

    space.html(loading_template());
    var data_id = 'unique_users';
    var local_data = localStorage.getItem(data_id);
    if (local_data){
        var data = JSON.parse(local_data);
        space.html(data_template(data))
    } else {
        $.ajax({
            url: '/dj/stat/json/unique_users'
        }).done(function(response){
            localStorage.setItem(data_id, JSON.stringify(response));
            space.html(data_template(response));
        }).fail(function() {
            space.html('Ошибка при загрузке данных')
        })
    }
}