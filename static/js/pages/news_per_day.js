$( document ).ready(function() {
    $('.nav-pills li').on('click',
        function(e){
            $('.active').removeClass('active');
            $(e.target).parent().addClass('active');
            load_data();
        }
    );
    load_data()
});

function load_data(){
    var period = $('.active').val();
    var data_template = _.template($('#news_per_day').html());
    var loading_template = _.template($('#loading').html());
    var space = $('#space');
    var data_id = 'news_per_day_period_' + period;

    var local_data = localStorage.getItem(data_id);
    if (local_data){
        var data = JSON.parse(local_data);
        space.html(data_template({rows: data}))
    } else {
        space.html(loading_template());
        $.ajax({
            url: '/dj/stat/json/news_per_day',
            data: {period: period}
        }).done(function(response){
            localStorage.setItem(data_id, JSON.stringify(response));
            space.html(data_template({rows: response}));
        }).fail(function() {
            space.html('Ошибка при загрузке данных')
        })
    }
}