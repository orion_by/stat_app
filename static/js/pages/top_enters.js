$( document ).ready(function() {
    $('.nav-pills li').on('click',
        function(e){
            $('.active').removeClass('active');
            $(e.target).parent().addClass('active');
            load_data();
        }
    );
    load_data()
});

function load_data(){
    var period = $('.active').val();
    var data_template = _.template($('#top_enters').html());
    var loading_template = _.template($('#loading').html());
    var space = $('#space');
    var data_id = 'top_enters_period_' + period;

    var local_data = localStorage.getItem(data_id);
    if (local_data){
        var data = JSON.parse(local_data);
        space.html(data_template(data))
    } else {
        space.html(loading_template());
        $.ajax({
            url: '/dj/stat/json/top_enters',
            data: {period: period}
        }).done(function(response){
            localStorage.setItem(data_id, JSON.stringify(response));
            space.html(data_template(response));
        }).fail(function() {
            space.html('Ошибка при загрузке данных')
        })
    }
}