
$( document ).ready(function() {
    var ajax_urls            = [
      "http://www.innoros.ru/api/v3/stat/autocompetition/",
      "http://innovationshop.ru/api/v3/stat/autocompetition/",
    ]
    var nid_container        = '.vpsd-filters-form__nid-container';
    var $nid_container       = $(nid_container);
    var nid_container_active = nid_container + '.active';
    var $nid_change_btn      = $('.vpsd-filters-form__nid-change');
    var nid_el               = '.vpsd-filters-form__nid';
    var nid_title_el         = '.vpsd-filters-form__nid-title';
    var $search              = $('.vpsd-nids-list__search');
    var $modal               = $('#vpsd-nids-list');
    var $nids_items          = $modal.find('.vpsd-nids-list__items');
    var $base_nids_items          = $modal.find('.vpsd-base-nids-list__items');
    var base_nids = [
        {nid: 'Энергетика', title: 'Энергетика', type: 'Экран'},
        {nid: 'Цифровой мир', title: 'Цифровой мир', type: 'Экран'},
        {nid: 'Транспорт', title: 'Транспорт', type: 'Экран'},
        {nid: 'Технологии', title: 'Технологии', type: 'Экран'},
        {nid: 'Сообщения', title: 'Сообщения', type: 'Экран'},
        {nid: 'Сообщение', title: 'Сообщение', type: 'Экран'},
        {nid: 'СОБЫТИЯ', title: 'СОБЫТИЯ', type: 'Экран'},
        {nid: 'Робототехника', title: 'Робототехника', type: 'Экран'},
        {nid: 'Поиск', title: 'Поиск', type: 'Экран'},
        {nid: 'Новости агентства', title: 'Новости агентства', type: 'Экран'},
        {nid: 'Новое сообщение', title: 'Новое сообщение', type: 'Экран'},
        {nid: 'Написать в редакцию', title: 'Написать в редакцию', type: 'Экран'},
        {nid: 'Медицина', title: 'Медицина', type: 'Экран'},
        {nid: 'Магазин', title: 'Магазин', type: 'Экран'},
        {nid: 'Магазин Инноваций', title: 'Магазин Инноваций', type: 'Экран'},
        {nid: 'Космос', title: 'Космос', type: 'Экран'},
        {nid: 'ИННОВАЦИИ В РОССИИ', title: 'ИННОВАЦИИ В РОССИИ', type: 'Экран'},
        {nid: 'ИННОВАЦИИ В МИРЕ', title: 'ИННОВАЦИИ В МИРЕ', type: 'Экран'},
        {nid: 'Избранное', title: 'Избранное', type: 'Экран'},
        {nid: 'Все новости', title: 'Все новости', type: 'Экран'},
        {nid: 'В России', title: 'В России', type: 'Экран'},
        {nid: 'В РЕГИОНЕ', title: 'В РЕГИОНЕ', type: 'Экран'},
        {nid: 'В мире', title: 'В мире', type: 'Экран'},
        {nid: 'Будущее', title: 'Будущее', type: 'Экран'},
        {nid: 'world_news', title: 'world_news', type: 'Экран'},
        {nid: 'WinnerActivityCreativity', title: 'WinnerActivityCreativity', type: 'Экран'},
        {nid: 'WinnerActivity', title: 'WinnerActivity', type: 'Экран'},
        {nid: 'WhyActivityBrain', title: 'WhyActivityBrain', type: 'Экран'},
        {nid: 'VideoActivity', title: 'VideoActivity', type: 'Экран'},
        {nid: 'SzondiVocabActivity', title: 'SzondiVocabActivity', type: 'Экран'},
        {nid: 'SzondiTestActivity', title: 'SzondiTestActivity', type: 'Экран'},
        {nid: 'SzondiStartActivity', title: 'SzondiStartActivity', type: 'Экран'},
        {nid: 'SzondiResultActivity', title: 'SzondiResultActivity', type: 'Экран'},
        {nid: 'SzondiInstrActivity', title: 'SzondiInstrActivity', type: 'Экран'},
        {nid: 'SzondiDiscrActivity', title: 'SzondiDiscrActivity', type: 'Экран'},
        {nid: 'StartActivityJung', title: 'StartActivityJung', type: 'Экран'},
        {nid: 'StartActivityIq', title: 'StartActivityIq', type: 'Экран'},
        {nid: 'StartActivityEQ', title: 'StartActivityEQ', type: 'Экран'},
        {nid: 'StartActivityCreativity', title: 'StartActivityCreativity', type: 'Экран'},
        {nid: 'StartActivityBrain', title: 'StartActivityBrain', type: 'Экран'},
        {nid: 'ShareActivity', title: 'ShareActivity', type: 'Экран'},
        {nid: 'send_to_innoros', title: 'send_to_innoros', type: 'Экран'},
        {nid: 'Search', title: 'Search', type: 'Экран'},
        {nid: 'ResultActivityEQ', title: 'ResultActivityEQ', type: 'Экран'},
        {nid: 'ResultActivityCreativity', title: 'ResultActivityCreativity', type: 'Экран'},
        {nid: 'ResultActivityBrain', title: 'ResultActivityBrain', type: 'Экран'},
        {nid: 'ResultActivity', title: 'ResultActivity', type: 'Экран'},
        {nid: 'PreferencesActivity', title: 'PreferencesActivity', type: 'Экран'},
        {nid: 'PostActivity', title: 'PostActivity', type: 'Экран'},
        {nid: 'PageResultsActivity', title: 'PageResultsActivity', type: 'Экран'},
        {nid: 'one_message', title: 'one_message', type: 'Экран'},
        {nid: 'news_region', title: 'news_region', type: 'Экран'},
        {nid: 'MainActivityTest', title: 'MainActivityTest', type: 'Экран'},
        {nid: 'MainActivityJung', title: 'MainActivityJung', type: 'Экран'},
        {nid: 'MainActivityEQ', title: 'MainActivityEQ', type: 'Экран'},
        {nid: 'MainActivityCreativity', title: 'MainActivityCreativity', type: 'Экран'},
        {nid: 'MainActivityBrain', title: 'MainActivityBrain', type: 'Экран'},
        {nid: 'MainActivity', title: 'MainActivity', type: 'Экран'},
        {nid: 'IntrActivityJung', title: 'IntrActivityJung', type: 'Экран'},
        {nid: 'IntrActivityCreativity', title: 'IntrActivityCreativity', type: 'Экран'},
        {nid: 'IntrActivityBrain', title: 'IntrActivityBrain', type: 'Экран'},
        {nid: 'InstrActivityEQ', title: 'InstrActivityEQ', type: 'Экран'},
        {nid: 'InstrActivityCreativity', title: 'InstrActivityCreativity', type: 'Экран'},
        {nid: 'innovations_in_russia_news', title: 'innovations_in_russia_news', type: 'Экран'},
        {nid: 'GoodsActivity', title: 'GoodsActivity', type: 'Экран'},
        {nid: 'GoodResultActivity', title: 'GoodResultActivity', type: 'Экран'},
        {nid: 'gcm_messages', title: 'gcm_messages', type: 'Экран'},
        {nid: 'favorite', title: 'favorite', type: 'Экран'},
        {nid: 'EventActivity', title: 'EventActivity', type: 'Экран'},
        {nid: 'EvaluateActivity', title: 'EvaluateActivity', type: 'Экран'},
        {nid: 'DescriptionActivity', title: 'DescriptionActivity', type: 'Экран'},
        {nid: 'CartActivity', title: 'CartActivity', type: 'Экран'},
        {nid: 'BuyActivity', title: 'BuyActivity', type: 'Экран'},
        {nid: 'all_news', title: 'all_news', type: 'Экран'},
        {nid: 'all_events', title: 'all_events', type: 'Экран'},
        {nid: 'agent_news', title: 'agent_news', type: 'Экран'},
        {nid: 'AboutActivity', title: 'AboutActivity', type: 'Экран'},
        {nid: 'about', title: 'about', type: 'Экран'},
    ];
    var showed_nids = [];

    append_base_nids();
    bind_nid_items();

    $(nid_el).bind('click', function(event) {
        // показываем попап с подбором "экрана"
        $modal.modal('show');
        $nid_container.removeClass('active');
        $nid_container.has(this).addClass('active');
    });

    $search.bind('keydown', function(event){
        console.log('keydown');
        var str = $(this).val();
        $nids_items.html('');
        showed_nids = [];
        get_nids(str);
    });

    set_autocomplete_source = function(data) {
        // console.log(data);
        // append_nids(data);
    }

    function append_nids(data) {
        if (data) {
            for (var i = 0; i < data.length; i++) {
                if ($.inArray(data[i].nid, showed_nids) !== -1) {
                    continue;
                }
                var nid_item = render_nid(data[i]);
                $nids_items.append(nid_item);
                showed_nids.push(data[i].nid)
            }
        }
        bind_nid_items();
    }

    function bind_nid_items() {
        $('.vpsd-nids-items__item-link').not('.vpsd-processed').bind('click', function(event){
            event.preventDefault();
            event.stopPropagation();

            var nid_value = $(this).attr('data-nid');
            var nid_title = $(this).text();
            var value =  nid_title + ' [nid:' + nid_value + ']'
            $(nid_container_active).find(nid_el).val(value);
            $(nid_container_active).find(nid_title_el).html(nid_title);
            $modal.modal('hide');
        }).addClass('vpsd-processed');
    }

    function render_nid(obj) {
        // из-за того, что в бд статистики для товаров идентификатором служит заголовок, а у остальных материалов nid
        nid = obj.nid;
        if (obj.type == 'product') {
            nid = obj.title
        }
       return '<li class="vpsd-nids-items__item"><a href="#" class="vpsd-nids-items__item-link" data-nid="'+nid+'">'+obj.title+' ['+obj.type+']</a></li>'
    }
    function render_nid_of_product(obj) {
       return '<li class="vpsd-nids-items__item"><a href="#" class="vpsd-nids-items__item-link" data-nid="'+nid+'">'+obj.title+' ['+obj.type+']</a></li>'
    }

    function append_base_nids() {
        for (var i = 0; i < base_nids.length; i++) {
            var nid_item = render_nid(base_nids[i]);
            $base_nids_items.append(nid_item);
        }
    }

    function get_nids(str) {
      for (var i = 0; i < ajax_urls.length; i++) {
        jQuery.ajax({
            type: "GET",
            url: ajax_urls[i] + str,
            dataType: "jsonp", 
            jsonp: 'callback',
            jsonpCallback: 'set_autocomplete_source',
            success: function (data) {
                append_nids(data);
            },
            error: function(data) {
                console.log(ajax_urls[i]);
                console.log('error');
            },
        });
      }
    }

});
