$( document ).ready(function() {
    $('.datepicker').datepicker({
        language: 'ru',
        format: 'yyyy-mm-dd',
        startDate: '-30d',
        endDate: '0d',
        autoclose: true
    }).on('changeDate', function(e){
       load_data($(e.target).val());
    });
    $('.nav-pills li').on('click',
        function(e){
            $('.active').removeClass('active');
            $(e.target).parent().addClass('active');
            var date = $('#datepicker').val();
            if (date){
                load_data(date);
            }
        }
    );
});

function load_data(date){
    console.log(date);
    var period = $('.active').val();
    var data_template = _.template($('#new_users_stat').html());
    var loading_template = _.template($('#loading').html());
    var space = $('#space');

    space.html(loading_template());
    var data_id = 'new_users_stat' + date + '_' + period;
    var local_data = localStorage.getItem(data_id);
    if (local_data){
        var data = JSON.parse(local_data);
        space.html(data_template(data))
    } else {
        $.ajax({
            url: '/dj/stat/json/new_users_stat',
            data: {
                period: period,
                date: date
            }
        }).done(function(response){
            localStorage.setItem(data_id, JSON.stringify(response));
            space.html(data_template(response));
        }).fail(function() {
            space.html('Ошибка при загрузке данных')
        })
    }
}