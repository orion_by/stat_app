$( document ).ready(function() {
    $('.nav-pills li').on('click',
        function(e){
            $('.active').removeClass('active');
            $(e.target).parent().addClass('active');
        }
    );
    $('.collapse').on('show.bs.collapse', function (e) {
        load_data($(e.target));
    })
});

function load_data(target){
    var user_id = target.attr('data-user_id');
    var data_template = _.template($('#actions_random_user').html());
    var loading_template = _.template($('#loading').html());
    var space = target.find('.panel-body');

    space.html(loading_template());
    var data_id = 'actions_random_user' + user_id;
    var local_data = localStorage.getItem(data_id);
    if (local_data){
        var data = JSON.parse(local_data);
        space.html(data_template({rows: data}))
    } else {
        $.ajax({
            url: '/dj/stat/json/actions_random_user',
            data: {user: user_id}
        }).done(function(response){
            localStorage.setItem(data_id, JSON.stringify(response));
            space.html(data_template({rows: response}));
        }).fail(function() {
            space.html('Ошибка при загрузке данных')
        })
    }
}