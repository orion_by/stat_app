#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

# import the Auth Helper class
import api_v3_auth

from apiclient.errors import HttpError
from oauth2client.client import AccessTokenRefreshError


def get_first_profile_id(service):
    # Get a list of all Google Analytics accounts for this user
    accounts = service.management().accounts().list().execute()

    if accounts.get('items'):
        # Get the first Google Analytics account
        first_account_id = accounts.get('items')[0].get('id')

        # Get a list of all the Web Properties for the first account
        webproperties = service.management().webproperties().list(accountId=first_account_id).execute()

        if webproperties.get('items'):
            # Get the first Web Property ID
            first_webproperty_id = webproperties.get('items')[0].get('id')

            # Get a list of all Views (Profiles) for the first Web Property of the first Account
            profiles = service.management().profiles().list(
                accountId=first_account_id,
                webPropertyId=first_webproperty_id).execute()

            if profiles.get('items'):
                # return the first View (Profile) ID
                return profiles.get('items')[0].get('id')

    return None


def get_results(service, profile_id):
  # Use the Analytics Service Object to query the Core Reporting API
    return service.data().ga().get(
        dimensions='ga:landingScreenName',
        ids='ga:' + profile_id,
        start_date='2014-04-01',
        end_date='2014-04-16',
        metrics='ga:visitors',
        sort='-ga:visitors',
        max_results='10',
        ).execute()


def print_results(results):
    # Print data nicely for the user.
    if results:
        print 'First View (Profile): %s' % results.get('profileInfo').get('profileName')
        for row in results.get('rows'):
            print '%s : %s' % (row[0], row[1])
    else:
        print 'No results found'


def main():
    service = api_v3_auth.initialize_service()
    try:
        profile_id = get_first_profile_id(service)
        if profile_id:
            results = get_results(service, profile_id)
            print_results(results)
    except TypeError, error:
        print ('There was an error in constructing your query : %s' % error)
    except HttpError, error:
        print ('Arg, there was an API error : %s : %s' % (error.resp.status, error._get_reason()))
    except AccessTokenRefreshError:
        print ('The credentials have been revoked or expired, please re-run the application to re-authorize')

if __name__ == '__main__':
    main()