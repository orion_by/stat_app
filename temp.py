# coding=utf-8


def get_month_users_from_file(filename):
    f = open(filename)
    users = map(lambda x: int(x.replace('"', "")), f.readlines())
    f.close()
    return set(users)

april = get_month_users_from_file(u'апрель.txt')
march = get_month_users_from_file(u'март.txt')
counter = 0
for user in april:
    if user in march:
        counter += 1
print counter
print u'Количество уникальных пользователей в марте: {0}'.format(len(march))
print u'Количество уникальных пользователей в апреле: {0}'.format(len(april))
print u'Пересечение пользователй в марте и апреле: {0}'.format(len(april&march))